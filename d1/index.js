console.log("Hello World");

// arithmetic operation
let x = 1397;
let y = 7831;

// addition
let sum = x + y;
console.log("result of of addition operator: " + sum);

// subtraction
let diff = x - y;
console.log("result of of subtraction operator: " + diff);

// multiplication
let pro = x * y;
console.log("result of of multiplication operator: " + pro);

// division
let qou = x / y;
console.log("result of of division operator: " + qou);

//modulo 
let remainder = y % x;
console.log("result of of modulo operator: " + remainder);



// assignment operator
// basic asignment operators (=)
// the assignment operators adds te value of the rioght operand to a variable and assigns the result to the variable
let assignmentNumber = 8;

// addition assignment operator
// used the current value of the variable and it adds a number (2) to itself. afterwards, reasssigns it as a new value
// assignmentNumber = assignmentNumber + 2
assignmentNumber += 2;
console.log("result of the addition assignment operator " + assignmentNumber);


// subtraction , multiplication, division (-=, *=, /=)

// subtraction
// assignmentNumber = assignmentNumber - 2
assignmentNumber -= 2;
console.log("result of the subtraction assignment operator " + assignmentNumber);

// multiplication
// assignmentNumber = assignmentNumber * 2
assignmentNumber *= 2;
console.log("result of the multiplication assignment operator " + assignmentNumber);

// division
// assignmentNumber = assignmentNumber * 2
assignmentNumber /= 2;
console.log("result of the division assignment operator " + assignmentNumber);


// multiple operators and parenthesis
/*
	when multiple operators are applied in a singe statement, it follows the PEMDAS rule (parenthesis, exponents, multiplication, divisio, addition and subtraction)

*/ 

let mdas = 1 + 2 - 3 * 4 / 5;
/*
3 * 4 = 12
mdas = 1 + 2 - 12 / 5
12 / 5 = 2.4
mdas = 1 + 2 - 2.4
1 + 2 = 3
mdas = 3 - 2.4
mdas = 0.6
*/
console.log("result of the mdas assignment operator " + mdas);

/*
2 - 3 = -1
4 / 5 = 0.8
pemdas = 1 + -1 * 0.8
pemdas = 1 + - 0.8
pemdas = 0.2
*/

// the order of the operation can be change by adding parenthesis of the logic
let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("result of the pemdas assignment operator " + pemdas);


let sample = 5 % 2 + 10;
/*
	5 % 2 = 1
	1 + 10 =11

*/
console.log(sample);

sample = 5  + 2 % 10;

/*
	2 % 10 = 2
	5 + 2 = 7

*/
console.log(sample);


// increment and decrement
// operators that add or subtract values by 1 and reassigns the value of the variable where the inceremen /decrement was applied to


let z = 1;

// pre-increment 
// the value of "z" is added by a value of 1 before returning the value and stroring it the varibalge "increment";
let preIncerment = ++z;
// console.log("result of the pre-incerment "+ preIncerment);

// console.log("result of the pre-incerment "+z );

// post-increment 
// the value of z is return and stored in the variable postIncrement then the value of z is increase by one

/*
	pre-increment : add agad
	post increment : magstore muna ng value then sa last i add pag nacall yung variable ulit
*/

/*
	pre-increment - add 1 first before reading the value 
	
	post-increment - reads the value first before adding 1
	
*/
let  postIncrement = z++;
console.log("result of the post-incerment "+ postIncrement);

console.log("result of the post-incerment "+ z );


let a = 2;

// pre-decrement 
// value of a is decrease by a value of 1 before returning and strong it in the variable preDecrement 

// let preDecrement = --a;
// console.log("result of the pre-incerment "+ preDecrement );
// console.log("result of the pre-incerment "+ a );

// post-decrement
// the value of a is returned adn stored in the variable postDecrement the the value of as is decrease by 1;
let postDecrement = a--;
console.log("result of the pre-incerment "+ postDecrement );
console.log("result of the pre-incerment "+ a );

/*
	pre-decrement - subtract 1 first before reading the value 
	
	post-decrement - reads the value first before subtract 1
	
*/


// type coercion
/*
	type coercion is the automatic or implicit conversion of values from one data type to another 
*/

let numA ='10'; //string
let numB = 12; //number

/*
	adding/concatinating  a  string a number will result as  a string
*/

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion); //string

let coercion1 = numA - numB;
console.log(coercion1);
console.log(typeof coercion1); //number


// non-coercion
let numC = 16;
let numD = 14;

let nonCoercion =numC +numD;
console.log(nonCoercion);
console.log(typeof nonCoercion); //number

// addition of number and boolean
/*
	 the result is a number
	 the boolean true is associated with the value of 1

	 while false is 0
*/

let numE = true + 1;
console.log(numE);
console.log(typeof numE); //number

let numF = false + 1;
console.log(numF);
console.log(typeof numF); //number

// comparison operator
let juan = "juan";
// equality operator (==)
/*
	checks wether the operands are equal/have the same content(not data type) 
	
	attemps to convert adn compare operands of different data types
	
	returns a boolean value (true/false)

*/
console.log(1==1); //t
console.log(1==2); //f
console.log(1=='1'); //t
console.log(1==true); //t
console.log('true'==true); //f

//compare two string that are the same 
console.log('juan'==juan); //t

// inequality operator (!=)
/*
	checks wether the operands are not equal/have different content

	attempts to convert compare opernads of diff data types
*/

console.log(1!=1); //f
console.log(1!=2); //t
console.log(1!='1'); //f
console.log(1!=true); //f
console.log('juan'!=juan); // f
console.log('juan'!='juan'); // f

// strict equality operator (===)
/*
	check wether the operands are equal /have the  same content or value and also compares the DATA TYPES of the 2 values
*/

console.log(1===1); //t
console.log(1===2); //f
console.log(1==='1'); //f
console.log(1===true); //f
console.log('true'===true); //f
console.log('juan'===juan); // t
console.log('juan'==='juan'); // t

//stric inequlit opertor (!==)
/*
	check wether the operands are not equal/have the same content also compers tehe data types of 2 values
*/
console.log(1!==1); //f
console.log(1!==2); //t
console.log(1!=='1'); //t
console.log(1!==true); //t
console.log('true'!==true); //t
console.log('juan'!==juan); // f
console.log('juan'!=='juan'); // f

// relational operator
// returen a boolean value
let j = 50;
let k = 65;
// greater than operator (>)
let isGreaterThan = j > k;
console.log(isGreaterThan); //f

// less than operator (<)
let isLessThan = j < k;
console.log(isLessThan); //t

// greater than or equal to (>=)
let isGreaterThanEqual = j >= k;
console.log(isGreaterThanEqual); //f

// less than or equal to (<=)
let isLessThanEqual = j <= k;
console.log(isLessThanEqual); //t

//force coercion happen the string to number
let numStr ='30';
console.log(j>numStr); //t

let str = 'thirty';
// 50 is greater than NaN(not a number)
console.log(j>str); //f

// logical operator 
let isLegalAge = true;
let isRegistered = false;

// logical AND operator (&&)
// return true if all operans are true
// true && true = true
// true && false =false 
// false && false = true

let allRrequirmentsMet = isLegalAge && isRegistered;
console.log(allRrequirmentsMet); // f


// logical OR operator (||) double pipe

let someRequirementsMet = isLegalAge || isRegistered;
// return true if one of the operans are true
// true || true = true
// true || false =true 
// false || false = false
console.log(allRrequirmentsMet); // t


// logical not operator (!) exclamation point
let someRequirementsNotMet = !isRegistered;
console.log(someRequirementsNotMet); // t










